﻿using UnityEngine;
using System.Collections;

public class MouseClick : MonoBehaviour {

    private Animator anim;

    private bool isActive = false;

	// Use this for initialization
	void Start () {

        anim = GetComponent<Animator>();
        anim.SetBool("isMoving", false);
    }

    void OnMouseDown()
    {
        if (isActive == false)
        {
            Debug.Log("Hello!");
            anim.SetBool("isMoving", true);
            isActive = true;
        } else if (isActive == true)
        {
            anim.SetBool("isMoving", false);
            isActive = false;
        }
    }

    // Update is called once per frame
    void Update () {
    
	}
}
